  //// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ DEFINATION ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  \\\\\

          var w = window.innerWidth,
          h = window.innerHeight;
          var keyc = true,
            keys = true,
            keyt = true,
            keyr = true,
            keyx = true,
            keyd = true,
            keyl = true,
            keym = true,
            keyh = true,
            key1 = true,
            key2 = true,
            key3 = true,
            key0 = true
          var focus_node = null,
            highlight_node = null;
          var zoomScale = 1;
          var tags_queue = [],
            tags_indexBank = [],
            noneUniqTags = [];
          var max_stroke = 2.5;
          var min_zoom = 0.3,
              max_zoom = 32;
          var highlight_color = "#ffff00",
              highlight_trans = 0.07;
          var zooming=false;
          var min_opacity=0.025,
              min_fontSize=12;
          var color = d3.scale.linear()
            .domain([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
            .range(["#DE1B1B", "#FFA500", "#003BAE", "#DF52E4", "#00A86B", "#FFA500", "#606060", "#00A86B", "#FFA500", "#606060"]);
            //var color = d3.scale.category20().domain(d3.range([0, max_community_number]));
          var size = d3.scale.pow().exponent(1)
            .domain([1, 100])
            .range([8, 24]);
          var force = d3.layout.force()
            .linkDistance(50)
            .charge(-100000)
            .gravity(10)
            .size([w, h])
            .stop();
          var zoom = d3.behavior.zoom().scaleExtent([min_zoom, max_zoom])
          /*
              d3 kütüphanesiyle veri okundu.
              veriler ait oldukları nesnelere aktarıldı. --> düğümler, kenarlar
              atama işlemleri ekranda görselleştirildi.
          */
          d3.json("data/graph.json", function(error, graph) {

            // Count of idea number ex: "140 fikir mevcut"
            fillDropdown();

            graph.nodes.forEach(function(d) {
              d.inDegree = 0;
              d.outDegree = 0;
            });

            graph.links.forEach(function(d) {
              graph.nodes[d.source].outDegree += 1;
              graph.nodes[d.target].inDegree += 1;
              graph.nodes[d.target].weight= graph.nodes[d.source].outDegree+graph.nodes[d.target].inDegree ;
            });

            //Node ıd (actualy it's name) for community detection
            var node_data = [];
            graph.nodes.forEach(function(d) {
              node_data.push(d.indexID);
            });

            //Original edge data
            var edge_data = [];
            graph.links.forEach(function(d) {
              edge_data.push(d);
            });

            // Community data
            // community use node name (tag) and link between side to side
            var community = jLouvain().nodes(node_data).edges(edge_data);

            node_data = [];
            graph.nodes.forEach(function(d) {
              node_data.push(d);
            });
            // We create original node and link data inside of node_data & edge_data;

            //console.log(node_data);console.log(edge_data);


            //after community effect create new node data with community
            // community: community indise
            // index: node indis
            // key: node key
            // px,py: cordinates
            // value: single node
            var community_node_data = d3.entries(node_data);


            var community_assignment_result = community();
            for(var i=0;i<community_node_data.length;i++)
                community_node_data[i].community=community_assignment_result[i];

            node_data.forEach(function(d) { d.community=community_node_data[d.indexID].community; });
          force.nodes(node_data)
              .links(edge_data)
              .start();


            //this array keep one node each other note connection to information
            var linkedByIndex = {};
            graph.links.forEach(function(d) { linkedByIndex[d.source.index + "," + d.target.index] = true; });

            //Normalization process all nodes weight will be normalized
            var max_weight = d3.max(edge_data, function(d) { return d.weight; });
            var line_scale = d3.scale.linear().domain([0, max_weight]).range([0, 2]);
            var opacity_scale=d3.scale.linear().domain([0,max_weight]).range([0,1]);

            var max_size=d3.max(node_data, function(d) {return d.size; });
            var text_scale=d3.scale.linear().domain([0,max_size]).range([8,48]);
            var node_scale=d3.scale.linear().domain([0,max_size]).range([3,30]);
            // var node_opacity_scale=d3.scale.linear().domain([0,max_size]).range([0.5,1]);
          var svg = d3.select("body")
            .append("svg")
            .attr("width", w)
            .attr("height", h)
            .call(d3.behavior.zoom().scaleExtent([0.5, 2.8]).on("zoom", function() {
                  zoomScale = d3.event.scale;
                  svg.attr("transform", "translate(" + d3.event.translate + ")" + " scale(" + zoomScale + ")");
                  //g.selectAll("text").data(tags_indexBank);

                  if(focus_node==null)
                    if (zoomScale >2.0) {
                      zooming=true;
                      g.selectAll("text").style("font-size", function(d) { return text_scale(d.size)+"px";});
                    }else if(zoomScale<0.75){
                    	zooming=false;
                    	g.selectAll("text").style("font-size", function(d) { return (text_scale(d.size*zoomScale) > min_fontSize)?text_scale(d.size)+"px":"0px";});
                    } else {
                      zooming=false;
                      g.selectAll("text").style("font-size", function(d) { return (text_scale(d.size) > min_fontSize)?text_scale(d.size)+"px":"0px";});
                    }
            })).append("g");
          var g = svg.append("g");
          var fikir = document.getElementById("dropdownmenu");// Fikirlerin gösterileceği containerın ve içereceği fikrin (button) atanması
          var btnIdea = document.createElement("button");

  //// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  ~ ~ ~ END ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ \\\\\

  //// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ INITIAL VALUES ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  \\\\\

          ////   -------------------   LINK   ----------------------------   \\\\
          // this data funct set edge_Data attention for source and target

          var link = g.selectAll(".link")
            .data(edge_data)
            .enter().append("line")
            .attr("class", "link")
            .style("opacity", function(d) { return opacity_scale(d.weight); })
            .style("stroke-width", function(d) { return line_scale(d.weight);})
            .style("stroke", function(d) { return color(d.source.community); });

          ////   ------------------   END LINK   -------------------------   \\\\

          ////   -------------------   CIRCLE   --------------------------   \\\\

          var circle = g.selectAll(".circle")
            .data(force.nodes())
            .enter()
            .append("circle")
            .attr("class", "node")
            .attr("r", function(d) { return node_scale(d.size); })
            .style("fill", function(d) { return color(d.community); })
            //.style("opacity", function(d) { return node_opacity_scale(d.size); })
            .on("mouseover", function(d) {svg.style("cursor", "pointer"); set_highlight(d); })
            .on("mousedown", function(d) { set_focus(d); })
            .on("mouseout", function(d) { exit_highlight(); });

          ////   -----------------   END CIRCLE   ------------------------   \\\\

          ////   --------------------   TEXT   ----------------------------   \\\\

          var text = g.selectAll(".text")
            .data(force.nodes())
            .enter()
            .append("text")
            .text(function(d) { return d.id; })
            .style("font-family","RobotoItalic")
            .style('fill', function(d) { return color(d.community); })
            .style('stroke',"#535353")
            .style('stroke-width','0.1')
            .style('-webkit-text-stroke',"0.1px #535353")
            .attr("dy", ".35em")
            .attr("dx", ".65em")
            .style("font-size", function(d) { return (text_scale(d.size) > min_fontSize) ? text_scale(d.size)+"px": 0; })
            .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });

          ////   --------------------  END TEXT   ----------------------------   \\\\

  //// ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  ~ ~ ~ END ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ \\\\\

          //reFill();
          // Fikirlerin yeniden ekrana basılması için yazıldı. Yenileme işlemi
          function reFill() {
            graph.ideas.forEach(function(d) {
              create_dropdown(d);
            });
            addPanelPadding();
          }

          // Attention! first condition a.index second condition a.indexID
          // İki düğümün birbirine bağlılığını kontrol etmek için yazıldı.
          function isConnected(a, b) {
            return (
              linkedByIndex[a.index + "," + b.index] ||
              linkedByIndex[b.index + "," + a.index] ||
              a.index == b.index);
          }

          // Bir düğümün herhangi bir bağlantısı olup olmadığını kontrol etmek için yazıldı.
          function hasConnections(a) {
            for (var property in linkedByIndex) {
              s = property.split(",");
              if ((s[0] == a.index || s[1] == a.index) && linkedByIndex[property]) return true;
            }
            return false;
          }

          // Düğümün üzerine gelme (Mouse in)
          function set_highlight(d) {

            if (focus_node !== null) d = focus_node;
            highlight_node = d;

            if (focus_node == null) {

              circle.style("stroke",function(o){ return (d==o)?highlight_color:"none";})

              circle.style("stroke-width", function(o) { return (d==o) ? max_stroke+"px": 0;});

              circle.style("opacity", function(o) { return isConnected(d, o) ? 1 :min_opacity; });

              text.style("opacity", function(o) { return isConnected(d, o) ? 1 : min_opacity; });

              text.style("font-size", function(o) { return text_scale(o.size)+"px"; });

              link.style("opacity", function(o) { return o.source.index == d.indexID || o.target.index == d.indexID ? 1 : min_opacity; });
              //Attention which edge front end the screen
              link.style("stroke", function(o) { return o.source.index == d.indexID || o.target.index == d.indexID ? o.source.size>o.target.size ? color(o.source.community) : color(o.target.community) : "none"; });


              text.style("opacity", function(o) { return isConnected(d, o) ? 1 : min_opacity; });

            }
          }

          // Düğümün üzerinden ayrılma (Mouse out)
          function exit_highlight() {

            highlight_node = null;

            if (focus_node === null) { // Daha önce hiç bir node basılmamış

                circle.style("opacity", 1);

                circle.style("stroke","none");

                text.style("opacity", 1);

                link.style("stroke", function(o) { return o.source.size>o.target.size ? color(o.source.community) : color(o.target.community); });

                link.style("opacity", function(d) { return opacity_scale(d.weight); })

                if(zooming)
                  text.style("font-size", function(d) { return text_scale(d.size)+"px"; });
                else
                  g.selectAll("text").style("font-size", function(d) { return ((text_scale(d.size*zoomScale) > min_fontSize)&&(text_scale(d.size) > min_fontSize))?text_scale(d.size)+"px":"0px";});
            }
          }

          // node tıklanma olayı
          function set_focus(d) {
              force.stop();
              focus_node = d;

              if (tags_queue.includes(d))
                tags_queue.splice(tags_queue.indexOf(d), 1);
              else {
                if (!noneUniqTags.includes(d) && noneUniqTags.length != 0) tags_queue = noneUniqTags = [];
                tags_queue.push(d);
              }

              markingNode(tags_queue);

              createTagsView(tags_queue);

              fillDropdown();

              cntrlInitialStatement();
          }

          force.on("tick", function() {

              // link.attr("x1", function(d) { return d.source.x; })
              //     .attr("y1", function(d) { return d.source.y; })
              //     .attr("x2", function(d) { return d.target.x; })
              //     .attr("y2", function(d) { return d.target.y; });

              circle.attr("cx", function(d) { return d.x;})
                    .attr("cy", function(d) { return d.y; });

              text.attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });

              // if(stopTime-startTime>5000)
              // force.stop();

          });
          force.on("end", function (){
            link.style("display","block");
            link.attr("x1", function(d) { return d.source.x; })
                .attr("y1", function(d) { return d.source.y; })
                .attr("x2", function(d) { return d.target.x; })
                .attr("y2", function(d) { return d.target.y; });
          });

          //Ortak ilişkili notları parlat;
          function markingNode(queue) {

            if (queue.length == 0) {
              circle.style("opacity", 1);
              text.style("opacity", 1);
              link.style("display", "inline");
              return;
            } else {
              noneUniqTags = findTargets(queue[0]);
              for (var i = 1; i < queue.length; i++)
                noneUniqTags = $.arrayIntersect(noneUniqTags, findTargets(queue[i]));
            }
            tags_indexBank = [];

            for (var element in noneUniqTags)
              tags_indexBank.push(noneUniqTags[element].index);

            circle.style("opacity", function(o) { return noneUniqTags.includes(o) ? 1 : highlight_trans; });

            circle.style("stroke", function(o) { return isConnected(focus_node, o) ? highlight_color : color(o.community);  });

            circle.style("stroke-width", function(o) {  return (tags_queue.includes(o)) ? max_stroke : 0; });

            link.style("opacity","1");

            link.style("display", function(o) { return (tags_indexBank.includes(o.source.index) && tags_indexBank.includes(o.target.index)) ? "inline" : "none"; });

            link.style("stroke", function(o) { return color(o.target.community); });//Attention which edge front end the screen

            text.style("opacity", function(o) { return noneUniqTags.includes(o) ? 1 : highlight_trans; });

            text.style("font-size", function(o) { return text_scale(o.size)+"px"; });

            text.style("opacity", function(o) { return noneUniqTags.includes(o) ? 1 : highlight_trans; });

            text.style('stroke-width','0.5');
          }

          // fikir containerin alt kısmına tıklanan düğümün tag halinin yazılması çift taraflı çalışır node-tag,tag-node
          function createTagsView(tags_queue) {
            document.getElementById("tags").innerHTML = '';
            fikir = document.getElementById("tags");
            var ul = document.createElement("ul");
            for (var i = 0 in tags_queue) {
              var li = document.createElement("li");
              var a = document.createElement("a");
              var span = document.createElement("span");
              span.className = "tagspan";
              span.textContent = tags_queue[i].id;
              li.appendChild(a);
              a.appendChild(span);
              ul.appendChild(li);
            }
            fikir.appendChild(ul);
            a_clickEvent();
          }

          // Taga tıklama olayı node tıklama olayı ile eşleştirildi çift taraflı çalışma gerçekleştirildi.
          function a_clickEvent() {
            document.getElementById("dropdownmenu").innerHTML = '';
            var elements = document.getElementsByClassName('tagspan');
            for (var i = 0, len = elements.length; i < len; i++)
              elements[i].onclick = function() {
                for (var infoNode in graph.nodes)
                  if (this.textContent == graph.nodes[infoNode].id)
                    var tempNode = graph.nodes[infoNode];
                tags_queue.splice(tags_queue.indexOf(tempNode), 1);
                markingNode(tags_queue);
                createTagsView(tags_queue);
                fillDropdown();
              }
              cntrlInitialStatement();
          }

          // filtrelenmiş fikirleri getir.
          function getFilteredIdea() {
            if (tags_queue[0] != null && tags_queue[0].ideaID.length > 1) {
              var dummy = tags_queue[0].ideaID.split(",");
              for (var i = 1 in tags_queue)
                dummy = $.arrayIntersect(dummy, tags_queue[i].ideaID.split(","));
              return dummy;
            } else {
              if (tags_queue[0] != null) {
                var dummy = [];
                dummy[0] = tags_queue[0].ideaID;
                return dummy;
              }

            }
          }

          // Gidebilir nodeları getirir.
          function findTargets(d) {
            var tags = [];
            for (var infoNode in graph.nodes)
              if (d.id == graph.nodes[infoNode].id) {
                tags.push(graph.nodes[infoNode]);
                for (var infoLinks in graph.links)
                  if (graph.nodes[infoNode] == graph.links[infoLinks].source)
                    tags.push(graph.links[infoLinks].target);
              }
            return tags;
          }

          //Haritayı sıfırlamaya gerek var mı? Kontrol Edelim.
          function cntrlInitialStatement(){
            if (tags_queue.length == 0){
                focus_node=null;
                exit_highlight();
                text.style('stroke-width','0.1');
            }
          }

          // Fikirlerin bulunduğu bloğu temizleme
          function create_dropdown(d) {
            fikir = document.getElementById("dropdownmenu");

            btnIdea = document.createElement("button");//Akordion başlığı
            btnIdea.className = "accordion";
            btnIdea.style = "background-color: #f2f2f2; color:#111; background-image:none;position:relative;font-family: 'fonts/Roboto-Bold'; font-size: 18px;";
            var btnTxtContent = document.createElement("div");
            btnTxtContent.style = "width:80%;float:left;";
            var btnTxt = document.createTextNode(d.title);
            btnTxtContent.appendChild(btnTxt);

            var imgContent = document.createElement("div");
            imgContent.style = "width:15%;height:50%;float:right; position:absolute; top:15%; right:4%;";

            var imgBtwenness = document.createElement("img");
            imgBtwenness.setAttribute("src", "img/star.png");
            imgBtwenness.setAttribute('height', '25px');
            imgBtwenness.setAttribute('width', '25px');
            imgBtwenness.setAttribute("title", "Fikrin değeri");
            imgBtwenness.style = "margin-top:15%;margin-left:5px;";
            var txtBtwennessContent = document.createElement("p");
            txtBtwennessContent.className = "voteText";
            txtBtwennessContent.style = "height:50%;float:right;";

            var txtBtwenness = document.createTextNode(d.betweenness);
            txtBtwennessContent.appendChild(txtBtwenness);

            imgContent.appendChild(imgBtwenness);
            imgContent.appendChild(txtBtwennessContent);

            btnIdea.appendChild(imgContent);
            btnIdea.appendChild(btnTxtContent);

            fikir.appendChild(btnIdea);

            var divIdea = document.createElement("div");//Akordiyon içeriği
            divIdea.setAttribute("class", "panel hyphenate");
            divIdea.style = "text-align:justify;padding-left: 30px;padding-right:30px;padding-top:20px;padding-bottom:20px;display:block; height:auto; margin-bottom: 10px; margin-left: 10px; margin-right: 14px;background-color: white; border-radius: 0px 0px 10px 10px;font-family: 'fonts/Roboto-Bold'; font-size: 18px; font-weight: 300;  line-height: 24px;";


            var ideaFirstPart = document.createElement("p");
            var ideaColorPart = document.createElement("p"); //renklendirilecek
            ideaColorPart.style.color = "yellow";
            var ideaLastPart = document.createElement("p");
            var ideaResult = document.createElement("p");

            //burada renkelndircem kelimeyi buldum indis graphından
            var idea = "";
            var arrayIdea = "";


            var coloringTags = new Array();

            idea = d.idea.toLowerCase().replace(/,/gi, ", ");

            for (var i = 0; i < tags_queue.length; i++) {
              ///sorun iki noda tıklayınca iki kelimeyi de boyamaması.ona bakcam
              for (var j = 0; j < graph.highlight.length; j++)
                if (tags_queue[i].id === graph.highlight[j].root)
                  Array.prototype.push.apply(coloringTags, graph.highlight[j].raw);
            }

            arrayIdea = idea.split(" ");
            var text = "";
            for (var i = 0; i < arrayIdea.length; i++) {
              var coloringTextNode, span;
              if(i==0)arrayIdea[i]=arrayIdea[i].capitalize();
              if (coloringTags.includes(arrayIdea[i].toLowerCase()) || coloringTags.includes(arrayIdea[i].toLowerCase().substring(0, arrayIdea[i].length - 1))) {
                coloringTextNode = document.createTextNode(arrayIdea[i].trim());
                span = document.createElement("span");
                span.appendChild(coloringTextNode);
                text += span.outerHTML + " ";
              } else text += arrayIdea[i] + " ";
            }

            var span2 = document.createElement("div");
            span2.innerHTML = text;
            ideaResult.appendChild(span2);

            //buraya bakıcam
            divIdea.appendChild(ideaResult);
            fikir.appendChild(divIdea);
            //button_clickEvent();
          }

          // Fikir containerı içerisindeki herbir fikrin birbirinden ayırılması için boşluk ataması yapıldı.
          function addPanelPadding() {
            var panel = document.getElementsByClassName("panel");
            if (panel[panel.length - 1])
              panel[panel.length - 1].style = "text-align:justify;padding-left: 30px;padding-right:30px;padding-top:20px;padding-bottom:20px;text-indent: 15px;display:block; height:auto; margin-bottom: 10px; margin-left: 10px; margin-right: 14px;background-color: white; border-radius: 0px 0px 10px 10px;margin-bottom:100px;font-family: 'fonts/Roboto-Bold'; font-size: 18px; font-weight: 300;  line-height: 24px;";
          }

          // Fikir container içindeki fikrin başlığına basıldığında fikir içeriğinin gizlenme olayı. Şuan pasifte
          function button_clickEvent() {
            var acc = document.getElementsByClassName("accordion");
            for (var m = 0; m < acc.length; m++) {
              acc[m].onclick = function() {
                /* Toggle between adding and removing the "active" class,
                to highlight the button that controls the panel */
                this.classList.toggle("active");

                /* Toggle between hiding and showing the active panel */
                var panel = this.nextElementSibling;
                if (panel.style.display === "block") {
                  panel.style.display = "none";
                  this.style = "border-radius: 10px 10px 10px 10px; margin-bottom:10px;";
                } else {
                  panel.style.display = "block";
                  this.style = "border-radius: 10px 10px 5px 5px; margin-bottom:0px;";
                }
              }
            }
          }

          // Excell dosyası oluşturma ve indirme
          window.saveFile = function saveFile (Sayfa1,title) {
              var opts = [{sheetid:'idea2community',header:true}];
              var result = alasql('SELECT * INTO XLSX("'+title+'-idea2community.xlsx",?) FROM ?', [opts,[Sayfa1]]);
          }
          
          document.getElementById("downloadIcon").addEventListener("click", createIdea2WordMatrix);

          //createIdea2WordMatris row=idea, column=words
          function createIdea2WordMatrix(){
            var rows=new Array(graph.ideas.length);
            var indexX=0;
            graph.ideas.forEach(function(d){
              var words=d.words.split(",");
              var indexY=0;
              var cols=new Object();
              cols["Fikir"]=d.idea;
              graph.nodes.forEach(function(d){
                if(cols[(d.community+1)+". Community"]!=1)
                  cols[(d.community+1)+". Community"]=0;
                if(words.includes(d.id))
                  cols[(d.community+1)+". Community"]=1;
              });
              rows[indexX]=cols;
              indexX++;
            });
            var title="";
            if(window.location.pathname.split("/")[2]!="")title=window.location.pathname.split("/")[2];
           	else if(window.location.pathname.split("/")[1]!="")title=window.location.pathname.split("/")[1];
           	else if(window.location.pathname.split("/")[0]!="")title=window.location.pathname.split("/")[0];
            saveFile(rows,title);
          }

          // Ortak geçen elemanları bulur.
          $.arrayIntersect = function(a, b) {
            return $.grep(a, function(i) {
              return $.inArray(i, b) > -1;
            });
          };
          //
          // resize();
          // //window.focus();
          // d3.select(window).on("resize", resize);
          //
          // function resize() {
          //   var width = window.innerWidth,
          //     height = window.innerHeight;
          //   svg.attr("width", width).attr("height", height);
          //   force.size([(force.size()[0] + (width - w) / zoom.scale()) * 1.2, force.size()[1] + (height - h) / zoom.scale()]).resume();
          //   w = width;
          //   h = height;
          // }

          $('input[type=checkbox]').change(function() {
            switch (this.id) {
              //dataya eklenen kategoriler işlevsellik bakımında bu alana eklenmesi gerekmektedir. (Manual)
              // görsellik bakımından index.html sayfasına eklenmesi gerekmektedir.
              case "category1":
                keyc = !keyc;
                break;
              case "category2":
                keys = !keys;
                break;
              case "category3":
                keyt = !keyt;
                break;
            }
            link.style("display", function(d) {
              var flag = vis_by_type(d.source.type) && vis_by_type(d.target.type) && vis_by_node_score(d.source.score) && vis_by_node_score(d.target.score) && vis_by_link_score(d.score);
              linkedByIndex[d.source.index + "," + d.target.index] = flag;
              return flag ? "inline" : "none";
            });
            node.style("display", function(d) {
              return (key0 || hasConnections(d)) && vis_by_type(d.type) && vis_by_node_score(d.score) ? "inline" : "none";
            });
            text.style("display", function(d) {
              return (key0 || hasConnections(d)) && vis_by_type(d.type) && vis_by_node_score(d.score) ? "inline" : "none";
            });

            if (highlight_node !== null) {
              if ((key0 || hasConnections(highlight_node)) && vis_by_type(highlight_node.type) && vis_by_node_score(highlight_node.score)) {
                if (focus_node !== null) set_focus(focus_node);
                set_highlight(highlight_node);
              } else {
                exit_highlight();
              }
            }
          });

          // Fikir containerının doldurulması
          function fillDropdown() {
            if (getFilteredIdea()) {
              if (getFilteredIdea().length != 0) {
                document.getElementById("countOfIdea").innerHTML = "<font size='5em'>" + getFilteredIdea().length + " </font>fikir listelendi.";
              } else {
                document.getElementById("countOfIdea").innerHTML = "Fikir bulunamadı.";
              }

            } else {

              document.getElementById("countOfIdea").innerHTML = "<font size='5em'>" + graph.ideas.length + "</font> fikir mevcut.";
            }


            for (var id in getFilteredIdea()) {

              create_dropdown(graph.ideas[getFilteredIdea()[id]]); //burada fikirleri dropdowna yüklüyoruz
            }

            addPanelPadding();

            if (getFilteredIdea())
              if (getFilteredIdea().length == 0)
                document.getElementsByClassName("hasIdeas")[0].style.display = "inline";
              else
                document.getElementsByClassName("hasIdeas")[0].style.display = "none";
          }

          /* Arama barının ayarlamaları ve tanımlamaları */
          jQuery(".search-icon").click(function() {
            searchToggle(this, event);
          });

          jQuery(".close").click(function() {
            searchToggle(this, event);
          });

          jQuery(".reload").click(function() {
            // Yenile metodunun çağrılacağı yer.
            location.reload();
            keyc = true;
            keys = true;
            keyt = true;
            $('#category1').prop('checked', true);
            $('#category2').prop('checked', true);
            $('#category3').prop('checked', true);
            link.style("display", function(d) {
              var flag = vis_by_type(d.source.type) && vis_by_type(d.target.type) && vis_by_node_score(d.source.score) && vis_by_node_score(d.target.score) && vis_by_link_score(d.score);
              linkedByIndex[d.source.index + "," + d.target.index] = flag;
              return flag ? "inline" : "none";
            });
            node.style("display", function(d) {
              return (key0 || hasConnections(d)) && vis_by_type(d.type) && vis_by_node_score(d.score) ? "inline" : "none";
            });
            text.style("display", function(d) {
              return (key0 || hasConnections(d)) && vis_by_type(d.type) && vis_by_node_score(d.score) ? "inline" : "none";
            });
            document.getElementById("countOfIdea").innerHTML = "<font size='4em'>" + graph.ideas.length + "</font>fikir mevcut.";
            window.location.reload();
          });

          jQuery(".settings").click(function() {
            if (document.getElementsByClassName('winFilter')[0].style.display == "inline") {
              document.getElementsByClassName('winFilter')[0].style.display = "none";
              this.style = "border-bottom:none";
            } else {
              document.getElementsByClassName('winFilter')[0].style.display = "inline";
              this.style = "border-bottom: 2px solid #3498db";
            }
          });

          $("form").submit(function(event) {
            event.preventDefault();
            submitFn(this, event);
          });

          function searchToggle(obj, evt) {
            var container = $(obj).closest('.search-wrapper');
            if (!container.hasClass('active')) {
              container.addClass('active');
              evt.preventDefault();
            } else if (container.hasClass('active') && $(obj).closest('.input-holder').length == 0) {
              container.removeClass('active');
              container.find('.search-input').val('');
            }
          }

          function submitFn(obj, evt) {
            $(obj).find('.result-container').fadeOut(0);
            value = $(obj).find('.search-input').val().trim();
            var isWord = false;
            if (value.length)
              for (var infoNode in graph.nodes)
                if (value.trim().toLowerCase() === (graph.nodes[infoNode].id).trim().toLowerCase()){
                  isWord=true;
                  if (!tags_queue.includes(graph.nodes[infoNode])){
                      if(!noneUniqTags.includes(graph.nodes[infoNode]))
                        tags_queue=[];
                      tags_queue.push(graph.nodes[infoNode]);
                  }else
                    tags_queue.splice(tags_queue.indexOf(graph.nodes[infoNode]), 1);
                }

            if (isWord) {
              // arama işlemi yapıldığında aynı anda tıklanma fonksiyonunun özelliklerini kaybetmemesi için atama işlemleri yapıldı.
              focus_node = tags_queue[tags_queue.length - 1];
              highlight_node = tags_queue[tags_queue.length - 1];
              circle.style("stroke", function(o) { return tags_queue.includes(o)? "yellow":color(o.community);});
              circle.style("stroke-width", function(o) { return tags_queue.includes(o)? max_stroke: 0;});
              text.style("font-size", function(d) { return text_scale(d.size)+"px"; });
              link.style("opacity","1");
              link.style("stroke", function(o) { return color(o.target.community); });
              markingNode(tags_queue);
              createTagsView(tags_queue);
              fillDropdown();
              cntrlInitialStatement();
              $(obj).find('.search-input').val('');
              $(obj).find('.result-container').css("display:none");

            } else {
              if (value == '') {
                $(obj).find('.result-container').html('<span>Lütfen bir kelime giriniz.</span>');
                $(obj).find('.result-container').fadeOut(0);
                $(obj).find('.result-container').fadeIn(100);
                $(obj).find('.result-container').fadeOut(3000, function() { $(this).empty(); });
              } else {
                $(obj).find('.result-container').html('<span> Aradığınız kelimeyi bulamadık.</span>');
                $(obj).find('.result-container').fadeOut(0);
                $(obj).find('.result-container').fadeIn(100);
                $(obj).find('.result-container').fadeOut(3000, function() { $(this).empty(); });
              }

            }
            evt.preventDefault();
          }

          $(".search-icon").click(function() {
            $(".result-container").stop();
          });
          /*---------------------------------------------*/
          String.prototype.capitalize = function() {
             return this.charAt(0).toUpperCase() + this.slice(1);
          }
}); //d3 end

          // düğümlerin sınıfları var ise sınıflara göre ekranda gösterme veya gizleme. Şuanda pasif
          function vis_by_type(type) {
            switch (type) {
              case "kime":
                return keyc;
              case "neyle":
                return keys;
              case "nerede":
                return keyt;
              case "diamond":
                return keyr;
              case "cross":
                return keyx;
              case "triangle-down":
                return keyd;
              default:
                return true;
            }
          }

          // Verilerin sınıf bilgisi yok fakat score bilgisi var ise score bilgisine göre gösterme veya gizleme. Şuanda pasif [node]
          function vis_by_node_score(score) {
            if (isNumber(score)) {
              if (score >= 0.666) return keyh;
              else if (score >= 0.333) return keym;
              else if (score >= 0) return keyl;
            }
            return true;
          }

          // Verilerin sınıf bilgisi yok fakat score bilgisi var ise score bilgisine göre gösterme veya gizleme. Şuanda pasif [link]
          function vis_by_link_score(score) {
            if (isNumber(score)) {
              if (score >= 0.666) return key3;
              else if (score >= 0.333) return key2;
              else if (score >= 0) return key1;
            }
            return true;
          }

          function isNumber(n) {
            return !isNaN(parseFloat(n)) && isFinite(n);
          }

          // Fikir containerının gizlenmesi veya gösterilmesi default açık gelmektedir.
          function showIdeaButton(obj) {
            var showIdea;
            if (document.getElementById("showIdea").innerHTML.length == 0)
              showIdea = document.getElementById("showIdeaClose");
            else
              showIdea = document.getElementById("showIdea");
            var ideaPanel = document.getElementById("ideaPanel");
            if (ideaPanel.style.display == "none") {
              ideaPanel.style.display = "block";
              document.getElementById("showIdea").style.display = "none";
            } else {
              ideaPanel.style.display = "none";
              document.getElementById("showIdea").style.display = "block";
            }
          }
